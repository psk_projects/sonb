const logs = [];
const comunicationLogs = [];
let answer = "yes";
let err2 = true;
let err3 = true;
let socketName = "";

if ("undefined" === typeof window) {
    importScripts("/socket.io/socket.io.js");
}

const socket = io("http://localhost:3000");

this.addEventListener("message", function (e) {
    switch (e.data.start) {
        case "start":
            socketName = e.data.name;
            break;
        case "yes":
            answer = "yes";
            break;
        case "no":
            answer = "no";
            break;
        case "err1T":
            socket.connect();
            break;
        case "err1F":
            socket.disconnect();
            break;
        case "err2T":
            err2 = true;
            break;
        case "err2F":
            err2 = false;
            break;
        case "err3T":
            err3 = true;
            break;
        case "err3F":
            err3 = false;
            break;
        case "disconnect":
            socket.disconnect();
            console.log(`Rozlaczanie klienta ${socket.disconnected}`);
            break;
        default:
            this.postMessage({ start: "start", msg: "Nieokreslny warunek" });
    }
});

const handleErr = (value1, value2) => {
    logs.push({ prepare: value1 });
    comunicationLogs.push({ prepare: value2 });
    postMessage({ start: "logs", logs });
    postMessage({ start: "comunicationLogs", comunicationLogs });
};

socket.on("prepare", () => {
    logs.length = 0;
    comunicationLogs.length = 0;
    if (err3) {
        if (answer === "yes") {
            if (err2) {
                handleErr("prepare", "yes");
                socket.emit("yes", "tak");
            } else {
                handleErr("prepare", " ");
            }
        } else if (answer === "no") {
            handleErr("abort", "no");
            socket.emit("no", "nie");
        } else {
            this.postMessage("Nieokreślony prepare");
        }
    } else {
        handleErr("prepare", "error 3");
        socket.emit("yes", "error 3");
    }
});

socket.on("commit", () => {
    logs[0].commit = "commit";
    comunicationLogs[0].commit = "ack";
    postMessage({ start: "logs", logs });
    postMessage({ start: "comunicationLogs", comunicationLogs });
    socket.emit("ack", "ack");
});

socket.on("abort", () => {
    logs[0].abort = "abort";
    comunicationLogs[0].abort = "ack";
    postMessage({ start: "logs", logs });
    postMessage({ start: "comunicationLogs", comunicationLogs });
    socket.emit("ack", "ack");
});

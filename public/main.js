let serverWorker1;
let serverWorker2;
let serverWorker3;
let serverWorker4;
let serverWorker5;
let serverWorker6;
const btnConnected = document.querySelector(".connected");
const btnDisconnect = document.querySelector(".disconnect");
const btnPrepare = document.querySelector(".prepare");
const spnConnected = document.querySelector(".connected span + span");
const spnDisconnect = document.querySelector(".disconnect span + span");
const spnPrepare = document.querySelector(".prepare span + span");
const spnCount = document.querySelector(".count");
const spnMainServerLogs1 = document.querySelector(".mainServerLogs1");
const ulMainServerLogs2 = document.querySelector(".mainServerLogs2");
const ulServer1Logs2 = document.querySelector(".server1Logs2");
const ulServer2Logs2 = document.querySelector(".server2Logs2");
const ulServer3Logs2 = document.querySelector(".server3Logs2");
const ulServer4Logs2 = document.querySelector(".server4Logs2");
const ulServer5Logs2 = document.querySelector(".server5Logs2");
const ulServer6Logs2 = document.querySelector(".server6Logs2");
const ulServer1Logs1 = document.querySelector(".server1Logs1");
const ulServer2Logs1 = document.querySelector(".server2Logs1");
const ulServer3Logs1 = document.querySelector(".server3Logs1");
const ulServer4Logs1 = document.querySelector(".server4Logs1");
const ulServer5Logs1 = document.querySelector(".server5Logs1");
const ulServer6Logs1 = document.querySelector(".server6Logs1");
const btnAnswers = document.querySelectorAll(".answer");
const btnErr1 = document.querySelectorAll(".err1");
const btnErr2 = document.querySelectorAll(".err2");
const btnErr3 = document.querySelectorAll(".err3");
const spnErr1 = document.querySelectorAll(".err1 span + span");
const spnErr2 = document.querySelectorAll(".err2 span + span");
const spnErr3 = document.querySelectorAll(".err3 span + span");
let active = false;
btnPrepare.disabled = !active;
btnDisconnect.disabled = !active;

const createList = (logs, ul, type) => {
    const li1 = document.createElement("li");
    const li2 = document.createElement("li");
    const li3 = document.createElement("li");
    const li4 = document.createElement("li");
    const li5 = document.createElement("li");
    const li6 = document.createElement("li");

    if (type === "comunicationLogs") {
        li1.textContent = logs[0].prepare && `Odebrano: prepare.`;
        li2.textContent = logs[0].prepare && `Wysłano: ${logs[0].prepare}`;
        li3.textContent = logs[0].commit && `Odebrano: commit.`;
        li4.textContent = logs[0].commit && `Wysłano:  ${logs[0].commit}`;
        li5.textContent = logs[0].abort && `Odebrano: abort.`;
        li6.textContent = logs[0].abort && `Wysłano:  ${logs[0].abort}`;
    } else if (type === "mainLogs") {
        li2.textContent = logs[0].answer && `${logs[0].answer}`;
        li3.textContent = logs[0].ack && `${logs[0].ack}`;
    } else {
        li1.textContent = logs[0].prepare && `${logs[0].prepare}`;
        li2.textContent = logs[0].commit && `${logs[0].commit}`;
        li3.textContent = logs[0].abort && `${logs[0].abort}`;
    }

    if (logs[0].prepare || logs[0].commit || logs[0].abort === undefined) {
        ul.textContent = "";
    }

    li1.textContent === "" ? null : ul.appendChild(li1);
    li2.textContent === "" ? null : ul.appendChild(li2);
    li3.textContent === "" ? null : ul.appendChild(li3);
    li4.textContent === "" ? null : ul.appendChild(li4);
    li5.textContent === "" ? null : ul.appendChild(li5);
    li6.textContent === "" ? null : ul.appendChild(li6);
};

const start = (e, ulServer1Logs, ulServer1Logs1) => {
    switch (e.data.start) {
        case "start":
            console.log(`Otrzymano odpowiedź: ${e.data.msg}`);
            break;
        case "logs":
            createList(e.data.logs, ulServer1Logs1, "logs");
            break;
        case "comunicationLogs":
            createList(
                e.data.comunicationLogs,
                ulServer1Logs,
                "comunicationLogs"
            );
            break;
        default:
            console.log("start default");
    }
};

const handleServerWorker = serverWorker => {
    serverWorker.forEach((server, index) => {
        server.addEventListener("message", function (e) {
            switch (index + 1) {
                case 1:
                    start(e, ulServer1Logs1, ulServer1Logs2);
                    break;
                case 2:
                    start(e, ulServer2Logs1, ulServer2Logs2);
                    break;
                case 3:
                    start(e, ulServer3Logs1, ulServer3Logs2);
                    break;
                case 4:
                    start(e, ulServer4Logs1, ulServer4Logs2);
                    break;
                case 5:
                    start(e, ulServer5Logs1, ulServer5Logs2);
                    break;
                case 6:
                    start(e, ulServer6Logs1, ulServer6Logs2);
                    break;
                default:
                    console.log("Błąd metody");
            }
        });

        server.addEventListener("error", function (e) {
            console.log(
                `Blad w pliku ${e.filename} w linii ${e.lineno}. Wiadmosc bledu: ${e.message}`
            );
        });
    });
};

btnConnected.addEventListener("click", () => {
    serverWorker1 = new Worker("serverWorker.js");
    serverWorker2 = new Worker("serverWorker.js");
    serverWorker3 = new Worker("serverWorker.js");
    serverWorker4 = new Worker("serverWorker.js");
    serverWorker5 = new Worker("serverWorker.js");
    serverWorker6 = new Worker("serverWorker.js");
    const serverWorker = [
        serverWorker1,
        serverWorker2,
        serverWorker3,
        serverWorker4,
        serverWorker5,
        serverWorker6,
    ];
    handleServerWorker(serverWorker);

    serverWorker1.postMessage({ start: "start", name: "Serwer 1" });
    serverWorker2.postMessage({ start: "start", name: "Serwer 2" });
    serverWorker3.postMessage({ start: "start", name: "Serwer 3" });
    serverWorker4.postMessage({ start: "start", name: "Serwer 4" });
    serverWorker5.postMessage({ start: "start", name: "Serwer 5" });
    serverWorker6.postMessage({ start: "start", name: "Serwer 6" });
    btnAnswer(serverWorker);
    btnErr1All(serverWorker);
    btnErr2All(serverWorker);
    btnErr3All(serverWorker);
    getServerCount();
    if (!active) {
        spnConnected.classList.add("activeMain");
        spnDisconnect.classList.remove("activeMain");
        spnPrepare.classList.add("activeMain");
        spnConnected.textContent = "Aktywny";
        spnPrepare.textContent = "Aktywny";
        spnDisconnect.textContent = "Nieaktywny";
        btnConnected.disabled = !active;
        btnDisconnect.disabled = active;
        btnPrepare.disabled = active;
        active = !active;
    }
});

btnDisconnect.addEventListener("click", () => {
    serverWorker1.postMessage({ start: "disconnect" });
    serverWorker2.postMessage({ start: "disconnect" });
    serverWorker3.postMessage({ start: "disconnect" });
    serverWorker4.postMessage({ start: "disconnect" });
    serverWorker5.postMessage({ start: "disconnect" });
    serverWorker6.postMessage({ start: "disconnect" });

    serverWorker1.terminate();
    serverWorker2.terminate();
    serverWorker3.terminate();
    serverWorker4.terminate();
    serverWorker5.terminate();
    serverWorker6.terminate();
    const serverWorker = [
        serverWorker1,
        serverWorker2,
        serverWorker3,
        serverWorker4,
        serverWorker5,
        serverWorker6,
    ];
    getServerCount();
    btnAnswer(serverWorker);
    btnErr1All(serverWorker);
    btnErr2All(serverWorker);
    btnErr3All(serverWorker);
    clear();
    if (active) {
        spnDisconnect.classList.add("activeMain");
        spnConnected.classList.remove("activeMain");
        spnPrepare.classList.remove("activeMain");
        spnConnected.textContent = "Nieaktywny";
        spnPrepare.textContent = "Nieaktywny";
        spnDisconnect.textContent = "Aktywny";
        btnConnected.disabled = !active;
        btnDisconnect.disabled = active;
        btnPrepare.disabled = active;
        active = !active;
    }
});

btnPrepare.addEventListener("click", () => {
    fetch("http://localhost:3000/prepare", {
        method: "post",
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then(res => res.json())
        .then(res => {
            spnCount.textContent = res.countClient;
            createList([res], ulMainServerLogs2, "mainLogs");
            spnMainServerLogs1.textContent = res.msg;
        })
        .catch(error => console.log("Błąd: ", error));
    clear();
});

const btnAnswer = serverWorker => {
    let answer1 = false;
    let answer2 = false;
    let answer3 = false;
    let answer4 = false;
    let answer5 = false;
    let answer6 = false;
    const answer = [answer1, answer2, answer3, answer4, answer5, answer6];
    btnAnswers.forEach((btn, index) => {
        btn.addEventListener("click", () => {
            if (answer[index]) {
                answer[index] = !answer[index];
                serverWorker[index].postMessage({ start: "yes" });
                btn.textContent = "tak";
            } else {
                answer[index] = !answer[index];
                serverWorker[index].postMessage({ start: "no" });
                btn.textContent = "nie";
            }
        });
        btn.classList.remove("active");
        btn.disabled = active;
        btn.textContent = "tak";
    });
};

const btnErr1All = serverWorker => {
    let err1 = false;
    let err2 = false;
    let err3 = false;
    let err4 = false;
    let err5 = false;
    let err6 = false;
    const err = [err1, err2, err3, err4, err5, err6];
    btnErr1.forEach((btn, index) => {
        btn.addEventListener("click", function () {
            if (!err[index]) {
                err[index] = !err[index];
                serverWorker[index].postMessage({ start: "err1F" });
                this.classList.add("active");
                spnErr1[index].textContent = "Aktywny";
            } else {
                err[index] = !err[index];
                serverWorker[index].postMessage({ start: "err1T" });
                this.classList.remove("active");
                spnErr1[index].textContent = "Nieaktywny";
            }
            getServerCount();
        });
        btn.classList.remove("active");
        btn.disabled = active;
        spnErr1[index].textContent = "Nieaktywny";
    });
};

const btnErr2All = serverWorker => {
    let err1 = false;
    let err2 = false;
    let err3 = false;
    let err4 = false;
    let err5 = false;
    let err6 = false;
    const err = [err1, err2, err3, err4, err5, err6];
    btnErr2.forEach((btn, index) => {
        btn.addEventListener("click", function () {
            if (!err[index]) {
                err[index] = !err[index];
                serverWorker[index].postMessage({ start: "err2F" });
                spnErr2[index].textContent = "Aktywny";
                this.classList.add("active");
            } else {
                err[index] = !err[index];
                serverWorker[index].postMessage({ start: "err2T" });
                spnErr2[index].textContent = "Nieaktywny";
                this.classList.remove("active");
            }
        });
        btn.classList.remove("active");
        btn.disabled = active;
        spnErr2[index].textContent = "Nieaktywny";
    });
};

const btnErr3All = serverWorker => {
    let err1 = false;
    let err2 = false;
    let err3 = false;
    let err4 = false;
    let err5 = false;
    let err6 = false;
    const err = [err1, err2, err3, err4, err5, err6];
    btnErr3.forEach((btn, index) => {
        btn.addEventListener("click", function () {
            if (!err[index]) {
                err[index] = !err[index];
                serverWorker[index].postMessage({ start: "err3F" });
                spnErr3[index].textContent = "Aktywny";
                this.classList.add("active");
            } else {
                err[index] = !err[index];
                serverWorker[index].postMessage({ start: "err3T" });
                spnErr3[index].textContent = "Nieaktywny";
                this.classList.remove("active");
            }
        });
        btn.classList.remove("active");
        btn.disabled = active;
        spnErr3[index].textContent = "Nieaktywny";
    });
};

const getServerCount = () => {
    fetch("http://localhost:3000/count", {
        method: "post",
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then(res => res.json())
        .then(res => {
            spnCount.textContent = res;
        })
        .catch(error => console.log("Błąd: ", error));
};

const clear = () => {
    ulMainServerLogs2.textContent = "";
    spnMainServerLogs1.textContent = "";
    ulServer1Logs1.textContent = "";
    ulServer2Logs1.textContent = "";
    ulServer3Logs1.textContent = "";
    ulServer4Logs1.textContent = "";
    ulServer5Logs1.textContent = "";
    ulServer6Logs1.textContent = "";
    ulServer1Logs2.textContent = "";
    ulServer2Logs2.textContent = "";
    ulServer3Logs2.textContent = "";
    ulServer4Logs2.textContent = "";
    ulServer5Logs2.textContent = "";
    ulServer6Logs2.textContent = "";
};

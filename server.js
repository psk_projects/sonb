const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const HOST = "127.0.0.1";
const PORT = 3000;

app.use(express.static("public"));
app.use(express.json());

const connectedServerId = [];
let countYes = 0;
let countClient = 0;
let countAnswer = 0;
const msgAll = [];

let logs = {
    prepare: "",
    answer: "",
    ack: "",
    msg: "",
    countClient: 0,
};

app.post("/prepare", function (req, res) {
    socket.emit("prepare", "przygotuj");
    setTimeout(() => checkVoices(res), 2000);
});

app.post("/count", function (req, res) {
    setTimeout(() => {
        res.send(JSON.stringify(countClient));
    }, 300);
});

const socket = io.on("connection", socket => {
    connectedServerId.push({ id: socket.id });
    countClient++;

    socket.on("yes", msg => {
        countYes++;
        countAnswer++;
        msg === "tak" ? null : msgAll.push(msg);
    });

    socket.on("no", () => countAnswer++);
    socket.on("ack", () => (logs.ack = "end"));
    socket.on("disconnect", () => countClient--);

    return socket;
});

const checkVoices = res => {
    if (msgAll[0]) {
        msgAll.length = 0;
        err(
            "Wystąpił błąd: Transakcja zostaje wycofana. Serwer odpowiedział złym komunikatem."
        );
        return res.send(logs);
    }

    if (countYes === countClient && countClient === 6) {
        commit();
    } else if (countClient < 6 && countClient !== 0) {
        err(
            "Wystąpił błąd: Transakcja zostaje wycofana. Utrata połączenia z serwerem."
        );
    } else if (countAnswer < countClient) {
        err(
            "Wystąpił błąd: Transakcja zostaje wycofana. Serwer nie odpowiada."
        );
    } else if (countAnswer - countYes !== 0) {
        err(
            "Brak błędów: Głosowanie zostaje odrzucone. Wystąpiły głosy przeciwne."
        );
    } else {
        console.log("błąd");
    }

    logs.countClient = countClient;
    res.send(logs);
};

const err = msg => {
    countAnswer = 0;
    countYes = 0;
    io.emit("abort", "odrzuc");
    logs.prepare = "yes";
    logs.answer = "abort";
    logs.ack = "end";
    logs.msg = msg;
};

const commit = () => {
    countYes = 0;
    countAnswer = 0;
    io.emit("commit", "zatwierdz");
    logs.msg = "Brak błędów. Komunikacja jest poprawana.";
    logs.prepare = "yes";
    logs.ack = "end";
    logs.answer = "commit";
};

server.listen(PORT, HOST, () =>
    console.log(`Działa na adresie ${HOST} i porcie ${PORT}`)
);
